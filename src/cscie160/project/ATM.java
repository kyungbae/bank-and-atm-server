package cscie160.project;
import java.rmi.*;

/**
 *	This is an ATM interface that extends {@code Remote} This interface is 
 *	implemented by {@code ATMImpl}
 *
 *	
 *	@author	Kyung Bae Park
 */
public interface ATM extends Remote {

	/**
	 *	The signature of {@code deposit} method
	 *
	 *	@param	accountinfo	AccountInfo
	 *	@param	amount		float value
	 *	@throws	RemoteException
	 *	@throws	Exception
	 */
	 public void deposit(AccountInfo accountinfo, float amount) throws RemoteException, Exception;
    
    	/**
	 *	The signature of {@code withdraw} method
	 *
	 *	@param	accountinfo	AccountInfo
	 *	@param	amount		float value
	 *	@throws	RemoteException
	 *	@throws	Exception
	 */
	 public void withdraw(AccountInfo accountinfo, float amount) throws RemoteException, Exception;
    
    	/**
	 *	The signature of {@code getBalance} method
	 *
	 *	@param	accountinfo	AccountInfo
	 *	@return	amount
	 *	@throws	RemoteException
	 *	@throws	Exception
	 */
	 public float getBalance(AccountInfo accountinfo) throws RemoteException, Exception;
	 
	/**
	 *	The signature of {@code transfer} method
	 *	
	 *	@param	sourceinfo	AccountInfo
	 *	@param	targetinfo	AccountInfo
	 *	@param	amount	float
	 *	@throws	RemoteException
	 *	@throws	Exception
	 */
	 public void transfer(AccountInfo sourceinfo, AccountInfo targetinfo, float amount) throws RemoteException, Exception;
    
	/**
	 *	The signature of {@code addClient} method
	 *
	 *	@param	client	ATMListener
	 *	@throws	RemoteException
	 */
	 public void addClient(ATMListener client) throws RemoteException;
    
}

