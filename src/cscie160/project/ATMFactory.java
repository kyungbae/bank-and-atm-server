package cscie160.project;

import java.rmi.*;

/**
 *	{@code ATMFactory} is an interface.It has one method, {@code getATM} that
 *	returns an {@code ATM} instance. 
 *
 *	@author		Kyung Bae park
 */
public interface ATMFactory extends Remote{
	public ATM getATM() throws RemoteException;
}
