package cscie160.project;
import java.rmi.*;
import java.rmi.server.*;
/**
 *	{@code ATMFactoryImpl} is a class that implements ATMFactory interface. 
 *	It creates a {@code ATMImpl} object and has a method {@code getATM} 
 *	to return the {@code ATMImpl} object as an instance of {@code ATM}.
 *
 *	@author		Kyung Bae park
 */
public class ATMFactoryImpl extends UnicastRemoteObject implements ATMFactory{
	
	private ATM atmimpl = null;
	
	/**
	 *	The constructor of {@code ATMFactoryImpl}. It creates an object 
	 *	of {@code ATMImpl}. 
	 *
	 *	@throws	RemoteException
	 */
	public ATMFactoryImpl() throws RemoteException{
		atmimpl = new ATMImpl();
	}
	
	/**
	 *	{@code getATM} returns an instace of {@code ATMImple} as an {@code ATM}. 
	 */
	public ATM getATM() {
		return atmimpl;
	}
}
