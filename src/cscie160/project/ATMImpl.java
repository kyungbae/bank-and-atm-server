package cscie160.project;
import java.util.*;
import java.rmi.*;
import java.rmi.server.*;
import java.net.MalformedURLException;

/**
 *	{@code ATMImpl} is a class that implements ATM interface.This class has
 *	a collection of {@code Client} objects and notifies them with
*	a {@code TransactionNotification} object when there is an event.
 *	For processing a request from a {@code Client}, {@code ATMImpl} uses {@code Bank} 
 *	and {@code Security} in the rmiregistry.. 
 *	
 *	@author		Kyung Bae park
 */

public class ATMImpl extends UnicastRemoteObject implements ATM{
    
    /**
    *	{@code ATMImpl} is construstor of {@code ATMImpl} class.The constructor 
    *	create a {@code ATMListener} collection for possible clients, and looks up
    *	{@code Bank} rmi object and {@code Security} object in the rmiregistry. 
    *	
    *	
     */
     
     // Cash amount ATM holds
     private float cashOnHand;
     
     // a reference of remote Bank object
     private Bank bank;
     
     // a reference of remote Security objec
     private Security security;
     
     // a Collection of Client objects
     private HashSet<ATMListener> clients;
     
     private TransactionNotification tn;
     
    public ATMImpl() throws RemoteException{
	    
	    // Set the cash amount ATM holds
	    cashOnHand = 500f;
	    
	    // Create a collection
	    clients = new HashSet<ATMListener>();
	    System.out.println("ATMImpl is creating!");
	    
	    try {
			// Look for Bank object and assign it
		    	bank = (Bank) Naming.lookup("//localhost/bank");
			System.out.println("I found the Bank");
			
			// Look for Security object and assign it
			security = (Security) Naming.lookup("//localhost/security");
			System.out.println("I found the Security");
			
		} catch (MalformedURLException mue) {
			mue.printStackTrace();
		} catch (NotBoundException nbe) {
			nbe.printStackTrace();
		} catch (UnknownHostException uhe) {
			uhe.printStackTrace();
		} catch (RemoteException re) {
			re.printStackTrace();
		}

    }
    
	/**
	 *	The public method that takes a amount and an accoutInfo object
	 *	then, it authenticates the account, checks the authorization 
	 *	by using {@code Security} object, and adds up the amount to the balance
	 *	If there is a failure while authenticating and authorizing, it calls
	 *	{@code throwingException()}. After it completes a transaction, it
	 *	creates a TransactionNotification and notify to the client.
	 *	
	 *	@param	accountinfo	AccountInfo
	 *	@param	amount		floating value
	 *	@throws	RemoteException
	 *	@throws	Exception
	 *	
	 */	 
	 public void deposit(AccountInfo accountinfo, float amount) throws RemoteException, Exception {

		 Account account = null;
		 
		 // Authenticating and authrozing account
		 if (security.authenticatingAccount(accountinfo) & 
			 security.authorizingOperation(accountinfo, "Deposit")) {
		 		account = bank.getAccount(accountinfo);
				account.addAmount(amount);
				
				// Creating a TranscationNotification object
				tn = new TransactionNotification(account, null, "Deposit", amount);
				
				// Notify clients with TransactionNotification object
				notifyClients(tn);
		 } 
		 else {
			 // Call throwingException when there is an authenticating or authorizing problem
			 throwingException();
		 }
	 }
	 
	/**
	 *	The public method that takes a amount and an accoutInfo object
	 *	then, it authenticates the account, checks the authorization 
	 *	by using {@code Security} object, and takes the amount from the balance. 
	 *	If there is a failure while authenticating and authorizing, it calls
	 *	{@code throwingException()}. Also, the ATM machine doesn't have 
	 *	enough cash to pay, it also calls {@code throwingException()}.
	 *	After it completes a transaction, it creates 
	 *	a TransactionNotification and notify to the client.
	 *
	 *	@param	accountinfo	AccountInfo
	 *	@param	amount		floating value
	 *	@throws	RemoteException
	 *	@throws	Exception
	 *	
	 */	
	public void withdraw(AccountInfo accountinfo, float amount) throws RemoteException, Exception {
		Account account = null;
		
		// Authenticating and authrozing account
		if (security.authenticatingAccount(accountinfo) &
			security.authorizingOperation(accountinfo, "Withdraw")) {
			
			account = bank.getAccount(accountinfo);
			
			// Check account has enough balance and ATM has enough cash
			if ((account.getAmount() >= amount) & (cashOnHand >= amount)) { 
		
				account.reduceAmount(amount);
				cashOnHand -= amount;
				
				// Creating a TranscationNotification object
				tn = new TransactionNotification(account, null, "Withdraw", amount);
				
				// Notify clients with TransactionNotification object
				notifyClients(tn);
			} 
			else {
				// Call throwingException when there is not enouth money
				throwingException();
			}
		}
		else {
			// Call throwingException when there is an authenticating or authorizing problem
			throwingException();
		}
	}
	
	/**
	 *	The public getter method that returns the amount of the account.
	 *	It authenticates the account, checks the authorization 
	 *	by using {@code Security} object, and adds up the amount to the balance
	 *	If there is a failure while authenticating and authorizing, it calls
	 *	{@code throwingException()}.
	 *
	 *	@param	accountinfo	AccountInfo
	 *	@return			The Float object
	 *	@throws	RemoteException
	 *	@throws	Exception
	 */	
	public float getBalance(AccountInfo accountinfo) throws RemoteException, Exception {
		
		Account account = null;
		
		// Authenticating and authrozing account
		if (security.authenticatingAccount(accountinfo) & 
			security.authorizingOperation(accountinfo, "Balance")) {
				account = bank.getAccount(accountinfo);
				return account.getAmount();
			}
		else {
			// Call throwingException when there is an authenticating or authorizing problem
			throwingException();
		}
		return 0f;
	}
	
	/**
	 *	The public method that takes a amount and two accoutInfo objects
	 *	then, it authenticates the accounts, checks the authorization 
	 *	by using {@code Security} object, and takes the amount from the 
	 *	source account and deposits to the target account.. 
	 *	If there is a failure while authenticating and authorizing, it calls
	 *	{@code throwingException()}. Also, the source account doesn't 
	 *	enough balance to transfer, it calls {@code throwingException()}.
	 *	After it completes a transaction, it
	 *	creates a TransactionNotification and notify to the client.
	 *
	 *	@param	sourceinfo	AccountInfo
	 *	@param	targetinfo	AccountInfo
	 *	@param	amount		floating value
	 *	@throws	RemoteException
	 *	@throws	Exception
	 *	
	 */
	public void transfer(AccountInfo sourceinfo, AccountInfo targetinfo, float amount) throws RemoteException, Exception {
		
		Account source = null;
		Account target = null;
		
		// Authenticating and authrozing account
		if ((security.authenticatingAccount(sourceinfo) & security.authenticatingAccount(targetinfo)) &
		    (security.authorizingOperation(sourceinfo, "Withdraw") & security.authorizingOperation(targetinfo, "Deposit"))) {
			
			source = bank.getAccount(sourceinfo);
			target = bank.getAccount(targetinfo);
			
			// Check account has enough balance
			if (source.getAmount() >= amount) {

				source.reduceAmount(amount);
				target.addAmount(amount);
				
				tn = new TransactionNotification(source, target, "Transfer", amount);
				notifyClients(tn);
			}
			else {
				// Call throwingException when there is not enouth money
				throwingException();
			}
		} 
		else {
			// Call throwingException when there is an authenticating or authorizing problem
			throwingException();
			}
	}
	
	/**
	 *	The public method that adds a client to the collection
	 *	
	 *	@param	client	ATMListener
	 *	@throws	RemoteException
	 */
	public void addClient(ATMListener client) throws RemoteException {
		clients.add(client);
	}
	
	/**
	 *	The public method that notify the clients in the collection
	 *
	 *	@param	notification	TransactionNotification
	 *	@throws	RemoteException
	 *	@throws	Exception
	 */
	public void notifyClients(TransactionNotification notification) throws RemoteException{
		for (ATMListener client : clients) {
			client.handleNotification(notification);
		}
	}
	
	/**
	 *	The public method that creates a TransactionNotification object in case
	 *	there is a problem. 
	 *	
	 *	@throws	RemoteException
	 *	@throws	Exception
	 */
	public void throwingException() throws RemoteException, Exception {
		tn = new TransactionNotification(null, null, "Exception", 0f);
		notifyClients(tn);
		throw new Exception();
	}
}
