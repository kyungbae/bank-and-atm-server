package cscie160.project;
import java.rmi.*;

/**
 *	{@code ATMListener} is an interface that {@code Client} implements.
 *
 */
public interface ATMListener extends Remote {
	
	/**
	 *	The signature of {@code handleNotification} method
	 *
	 *	@param	tf	TransactinoNotification
	 *	@throws	RemoteException
	 */
	public void handleNotification(TransactionNotification tf) throws RemoteException;
}
