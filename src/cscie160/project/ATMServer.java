package cscie160.project;
import java.rmi.*;
import java.rmi.server.*;

/**
 *	{@code Server} starts up the server process. It creates an instance of 
 *	{@code ATMFactoryImpl} and register it with RMI registry. 
 */
public class ATMServer {
	
	public static void main(String[] args) {
		
		ATMFactory atmfactory = null;
		
		try {
			System.out.println("ATM Server is starting");
			
			// Create an instance of ATMFactoryImpl
			atmfactory = new ATMFactoryImpl();
			System.out.println("Created ATM factory impl");
			
			// Register atmfactory
			Naming.rebind("//localhost/atmfactory", atmfactory);
			System.out.println("ATMFactory rebinding is done");
			
			System.out.println("Server is waiting for a client");	
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
