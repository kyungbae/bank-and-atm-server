package cscie160.project;
import java.rmi.*;

/**
 *	{@code Account} is an interface that extends {@code Remote}. {@code AccountImpl}
.*	implements this interface.
 *	
 *	@author	Kyung Bae Park
 */
public interface Account extends Remote {
	
	/**
	 *	The signature of {@code getAmount()} method
	 *	
	 *	@throws	RemoteException
	 *	@return	float value
	 */
	public float getAmount() throws RemoteException;
	 
	 /**
	  *	The signature of {@code addAmount} method
	  *	
	  *	@param	amount	float
	  *	@throws	RemoteException	
	  */
	public void addAmount(float amount) throws RemoteException;
	
	/**
	 *	The signature of {@code reduceAmount} method
	 *
	 *	@param	amount	float
	 *	@throws RemoteException
	 */
	public void reduceAmount(float amount) throws RemoteException;
	
	/**
	 *	The sinagure of {@code getAccountInfo()} method
	 *	
	 *	@return	AccountInfo
	 *	@throws	RemoteException	
	 */
	public AccountInfo getAccountInfo() throws RemoteException;
	
	/**
	 *	The signature of {@code getDepositAuthorization()} method
	 *
	 *	@return	boolean	
	 *	@throws	RemoteException	
	 */
	public boolean getDepositAuthorization() throws RemoteException;
	
	/**
	 *	The signature of {@code getWithdrawAuthorization} method
	 *
	 *	@return	boolean	
	 *	@throws	RemoteException	
	 */
	public boolean getWithdrawAuthorization() throws RemoteException;
	
	/**
	 *	The signature of {@code getBalanceAuthorization} method
	 *
	 *	@return	boolean	
	 *	@throws	RemoteException	
	 */
	public boolean getBalanceAuthorization() throws RemoteException;
}
