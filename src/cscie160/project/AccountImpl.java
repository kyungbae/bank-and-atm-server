package cscie160.project;
import java.rmi.*;
import java.rmi.server.*;

/**
 *	{@code AccountImpl} is an implementation class of {@code Account} interface.
 *	
 *	@author	Kyung Bae Park
 */
public class AccountImpl extends UnicastRemoteObject implements Account {
	
    // floating variable to store the amount of an account
    private float amount;
    
    // AccountInfo variable to store an AccountInfo that has account number and PIN
    private AccountInfo accountinfo;
    
    /*
     *	Three operation authorizations this account has. 
     *	Deposit authorization is necessary to perform deposit operation
     *	Withdraw authorization is necessary to perform withdraw operation
     * 	Balance authorization is necessary to perform getBalance operation
     */
    private boolean depositAuth;
    private boolean withdrawAuth;
    private boolean balanceAuth;
    
    /**
     *	Constructor of {@code AccountImpl}. It creates an AccountImpl object with 
     *	following parameters; AccountInfo, amount, deposit authorization, withdraw authorization, and
     *	balance authorization.
     *
     *	@param	accountinfo	AccountInfo
     *	@param	a		float
     *	@param	da		boolean
     *	@param	wa		boolean
     *	@param	ba		boolean
     *
     *	@throws	RemoteException	
     */
     public AccountImpl(AccountInfo accountinfo, float a, boolean da, boolean wa, boolean ba) throws RemoteException{
	    this.accountinfo = accountinfo;
	    amount = a;
	    
	    depositAuth = da;
	    withdrawAuth = wa;
	    balanceAuth = ba;
     }
	
     /**
      *	Getter method that returns the amount as a float value
      *	
      *	@throws	RemoteException	
      *	@return	float
      */
     public float getAmount() throws RemoteException {
        return amount;
     }
	
     /**
      *	The method that adds the value of a to the amount
      *	
      *	@param	a	float
      *	@throws	RemoteException	
      */
     public void addAmount(float a) throws RemoteException {
        amount += a;
     }
	
     /**
      *	The method that reduces the amount as much as the value of a
      *
      *	@param	a	float
      *	@throws RemoteException
      */
     public void reduceAmount(float a) throws RemoteException {
        amount -= a;
     }
     
     /**
      *	The method that returns the AccountInfo object
      *	
      *	@return	AccountInfo
      *	@throws	RemoteException	
      */
     public AccountInfo getAccountInfo() throws RemoteException {
	    return accountinfo;
     }
    
     /**
      *	The method that returns the deposit authorization boolean value
      *
      *	@return	boolean	
      *	@throws	RemoteException	
      */
    public boolean getDepositAuthorization() throws RemoteException {
		return depositAuth;
    }
    
    /**
      *	The method that returns the withdraw authorization boolean value
      *
      *	@return	boolean	
      *	@throws	RemoteException	
      */
    public boolean getWithdrawAuthorization() throws RemoteException {
		return withdrawAuth;
    }
    
    /**
      *	The method that returns the balance authorization boolean value
      *
      *	@return	boolean	
      *	@throws	RemoteException	
      */
    public boolean getBalanceAuthorization() throws RemoteException {
		return balanceAuth;
    }
}
