package cscie160.project;
import java.io.*;

/**
 *	{@code AccountInfo} is class that holds accunt number and PIN. It implements
 *	{@code Serializable}. 
 */
public class AccountInfo implements Serializable {
	
	private int accountNumber;
	private int pin;
	
	/**
	*	Constructor. It takes two parameters; account number and PIN. 
	*	
	*	@param	accountNumber	int
	*	@param	pin	int
	*/
	public AccountInfo(int accountNumber, int pin){
		this.accountNumber = accountNumber;
		this.pin = pin;
		
	}

	/**
	*	The method that returns account number
	*
	*	@return	accountNumber
	*/
	public int getAccountNumber(){
		return accountNumber;
	}
	
	/**
	*	The method that returns PIN.
	*
	*	@return pin
	*/
	public int getPIN(){
		return pin;
	}	
}
