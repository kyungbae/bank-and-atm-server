package cscie160.project;
import java.rmi.*;

/**
 *	This is a Bank interface that extends {@code Remote} This interface is 
 *	implemented by {@code BankImpl}
 *
 *	
 *	@author	Kyung Bae Park
 */
public interface Bank extends Remote {
	
	/**
	 *	The signature of {@code getAccount} method
	 *
	 *	@param	accountinfo	AccountInfo
	 *	@throws	RemoteException
	 */
	public Account getAccount(AccountInfo accountinfo) throws RemoteException;
}
