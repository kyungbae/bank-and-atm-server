package cscie160.project;
import java.util.*;
import java.rmi.*;
import java.rmi.server.*;

/**
 *	{@code BankImpl} implements interface {@code Bank}. It holds three accounts 
 *	to demonstate. 
 *
 */
public class BankImpl extends UnicastRemoteObject implements Bank {
	
	// Account collection
	private HashMap<Integer, Account> accounts;
	
	/**
	 *	Constructor of {@code BankImpl}. It creats three accounts with the
	 *	specification and put them in the account colletion
	 *
	 *	@throws	RemoteException
	 */
	public BankImpl() throws RemoteException{
		AccountInfo temp;
		accounts = new HashMap<Integer, Account>();
		
		// Create demo accounts
		temp = new AccountInfo(0000001, 1234);
		accounts.put(temp.getAccountNumber(), new AccountImpl(temp, 0f, true, true, true));

		temp = new AccountInfo(0000002, 2345);
		accounts.put(temp.getAccountNumber(), new AccountImpl(temp, 100f, true, false, true));
		
		temp = new AccountInfo(0000003, 3456);
		accounts.put(temp.getAccountNumber(), new AccountImpl(temp, 500f, false, true, true));
	}
	
	/**
	 *	The public getter method that returns {@code Account} object.
	 *
	 *	@param	accountinfo	AccountInfo
	 *	@return	Account	
	 *	@throws	RemoteException
	 */
	public Account getAccount(AccountInfo accountinfo) throws RemoteException {
		return accounts.get(accountinfo.getAccountNumber());
	}
}
