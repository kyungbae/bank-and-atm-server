package cscie160.project;
import java.rmi.*;
import java.rmi.server.*;

/**
 *	{@code BankServer} is a server that creates {@code BankImpl} object and 
 *	{@code SecurityImpl} object  and binds them to the rimregistry. 
 */
public class BankServer {
	
	public static void main(String[] args) {
		
		System.out.println("Bank server is starting up.");
		
		try {
			Bank bank = new BankImpl();
			System.out.println("Created Bank object");
		
			Naming.rebind("//localhost/bank", bank);
			System.out.println("Bank rebinding is done");
			
			Security security = new SecurityImpl(bank);
			System.out.println("Created Security object");
			
			Naming.rebind("//localhost/security", security);
			System.out.println("Security rebinding is done");
			
			System.out.println("Bank Server is in service now");	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
