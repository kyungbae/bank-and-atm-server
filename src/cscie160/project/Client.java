package cscie160.project;
import java.rmi.*;
import java.rmi.server.*;
  
  /**
   *	{@code Client} gets a {@code ATM} reference from the rmiregistry and 
   *	runs some tests.
   *
   *
   */
  public class Client extends UnicastRemoteObject implements ATMListener {    
     public static void main(String[] args) {
	     try {
		     ATMListener client;
		     ATM atm = null;
		     
		     // Getting ATMFactory object from the rmtregistry
		     ATMFactory factory = (ATMFactory)Naming.lookup("//localhost/atmfactory");
		     
		     // Getting ATM by uusing getATM()
		     atm = factory.getATM();
		     System.out.println("I got atm.");
		     
		     // Create a Client object 
		     client = new Client(atm);
		     System.out.println("Registered this client");
		     
		     // Run tests
		     testATM(atm);
		     
	     } catch (Exception e) {
		     e.printStackTrace();
	     }
	     System.out.println("All Test is done");
     }
     
     /**
      *	Constructor of {@code Client}. Whent it creates a Clinet object, it adds
      *	itself to {@code ATM}. 
      *
      *	@param	atm	ATM
      *	@throws	RemoteException	
      */
     public Client(ATM atm) throws RemoteException {
	     atm.addClient(this);
     }
     
     /**
      *	The public method that creates {@code AccoutInfo} and returns it.
      *
      *	@param	accountnumber	int
      *	@param	pin	int	
      *	@throws	Exception	
      *	@return	AccountInfo
      *
      */
     public static AccountInfo getAccountInfo(int accountnumber, int pin) throws Exception {
	     
	     return new AccountInfo(accountnumber, pin);
     }
     
     public static void testATM(ATM atm) {
	     if (atm!=null) {
		     printBalances(atm);
		     performTestOne(atm);
		     performTestTwo(atm);
		     performTestThree(atm);
		     performTestFour(atm);
		     performTestFive(atm);
		     performTestSix(atm);
		     performTestSeven(atm);
		     performTestEight(atm);
		     performTestNine(atm);
		     printBalances(atm);
	     }
   }
   
   public static void printBalances(ATM atm) {        
      try {
         System.out.println("Balance(0000001): "+atm.getBalance(getAccountInfo(0000001, 1234)));
         System.out.println("Balance(0000002): "+atm.getBalance(getAccountInfo(0000002, 2345)));
         System.out.println("Balance(0000003): "+atm.getBalance(getAccountInfo(0000003, 3456)));
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
   
   public static void performTestOne(ATM atm) {       
      try {
         atm.getBalance(getAccountInfo(0000001, 5555));
      } catch (Exception e) {
         System.out.println("Failed as expected: "+e);
      }
   }
   
   public static void performTestTwo(ATM atm) {       
      try {
         atm.withdraw(getAccountInfo(0000002, 2345), 500);
      } catch (Exception e) {
         System.out.println("Failed as expected: "+e);
      }
   }
   
   public static void performTestThree(ATM atm) {        
      try {
         atm.withdraw(getAccountInfo(0000001, 1234), 50);
      } catch (Exception e) {
         System.out.println("Failed as expected: "+e);
      }
   }
   
   public static void performTestFour(ATM atm) {         
      try {
         atm.deposit(getAccountInfo(0000001, 1234), 500);
      } catch (Exception e) {
         System.out.println("Unexpected error: "+e);
      }
   }
   
   public static void performTestFive(ATM atm) {         
      try {
         atm.deposit(getAccountInfo(0000002, 2345), 100);
      } catch (Exception e) {
         System.out.println("Unexpected error: "+e);
      }
   }
   
   public static void performTestSix(ATM atm) {       
      try {
         atm.withdraw(getAccountInfo(0000001, 1234), 100);
      } catch (Exception e) {
         System.out.println("Unexpected error: "+e);
      }
   }
   
   public static void performTestSeven(ATM atm) {        
      try {
         atm.withdraw(getAccountInfo(0000003, 3456), 300);
      } catch (Exception e) {
         System.out.println("Unexpected error: "+e);
      }
   }
   
   public static void performTestEight(ATM atm) {        
      try {
         atm.withdraw(getAccountInfo(0000001, 1234), 200);
      } catch (Exception e) {
         System.out.println("Failed as expected: "+e);
      }
   }
   
   public static void performTestNine(ATM atm) {        
      try {
         atm.transfer(getAccountInfo(0000001, 1234),getAccountInfo(0000002, 2345), 100);
      } catch (Exception e) {
         System.out.println("Unexpected error: "+e);
      }
   }
   
   /**
    *	Notification handler. It takes {@code TransactionNotification} as a parameter
    *	and it prints it out. 
    *
    *	@param	tn	TransactionNotification
    */
   public void handleNotification(TransactionNotification tn) {
	   System.out.println(tn);
   }

}
