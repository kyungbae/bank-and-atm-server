package cscie160.project;
import java.rmi.*;

/**
 *	{@code Security} is a interface that extends {@code Remote}. 
 *	It is implements by {@code SecurityImpl}. 
 */
public interface Security extends Remote {
	
	/**
	 *	The signature of {@code authenticateingAccount} method
	 *
	 *	@param	accountinfo	AccountInfo
	 *	@return	boolean
	 *	@throws	RemoteException	
	 */
	public boolean authenticatingAccount(AccountInfo accountinfo) throws RemoteException;
	
	/**
	 *	The signature of {@code authorizingOperation} method
	 *
	 *	@param	accountinfo	AccountInfo
	 *	@param	operation	String
	 *	@return boolean
	 *	@throws	RemoteException
	 */
	public boolean authorizingOperation(AccountInfo accountinfo, String operation) throws RemoteException;
	
}
