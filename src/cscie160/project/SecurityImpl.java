package cscie160.project;
import java.rmi.*;
import java.rmi.server.*;

/**
 *	{@code SecurityImpl} implements {@code Security} interface. 
 *
 */
public class SecurityImpl extends UnicastRemoteObject implements Security {
	
	Bank bank;
	Account account;
	
	/**
	 *	Constructor of {@code SecurityImpl}. It takse a reference of 
	 *	{@code Bank}.
	 *
	 */
	public SecurityImpl(Bank bank) throws RemoteException{
		this.bank = bank;
	}
	
	/**
	 *	The public method that takes {@code AccountInfo} object and authenticates
	 *	it by checking PIN. If it is authenticated, it returns true. 
	 *	Otherwise, it returns false.
	 *
	 *	@param	accountinfo	AccountInfo
	 *	@return	boolean
	 *	@throws	RemoteException	
	 */
	public boolean authenticatingAccount(AccountInfo accountinfo) throws RemoteException {
		
		
		account = bank.getAccount(accountinfo);		
		if (accountinfo.getPIN() == account.getAccountInfo().getPIN())
			return true;
		else
			return false;
	}
	
	/**
	 *	The public method that takes {@code AccountInfo} object, operation 
	 *	and authorizies the operation by checking Account's authorizaions. 
	 *	If it is authenticated, it returns true. Otherwise, it returns false.
	 *
	 *	@param	accountinfo	AccountInfo
	 *	@param	operation	String
	 *	@return	boolean
	 *	@throws	RemoteException	
	 */
	public boolean authorizingOperation(AccountInfo accountinfo, String operation) throws RemoteException {
		account = bank.getAccount(accountinfo);
		
		// Check each authorization based on the operation
		if ( operation.equals("Deposit"))
			return account.getDepositAuthorization();
		else if (operation.equals("Withdraw"))
			return account.getWithdrawAuthorization();
		else if (operation.equals("Balance"))
			return account.getBalanceAuthorization();
		else return false;
	}
}
