package cscie160.project;
import java.io.*;

/**
 *	{@code TransactionNotification} is a data class that includes information
 *	about a particular transaction like account, operation and amount.
 */
public class TransactionNotification implements Serializable {
	
	private Account sourceAccount;
	private Account targetAccount;
	private String message;
	private float amount;
	
	private String returnString;
	
	/**
	 *	Constructor of {@code TransactionNotification}. 
	 *
	 *	@param	source	Account
	 *	@param	target	Account
	 *	@param	m	String
	 *	@param	am	float
	 */
	public TransactionNotification(Account source, Account target, String m, float am) {
		sourceAccount = source;
		targetAccount = target;
		message = m;
		amount = am;
		
		
	}
	
	/**
	 *	{@code toString} is a text representation of {@code TransactionNotification}
	 *
	 *	@return	String
	 */
	public String toString(){
		returnString = "\n<TransactionNotification Message>";
		if (message.equals("Exception")) {
			return returnString;
		} 
		else {
			returnString += "\n";
			returnString += message;
			returnString += " $" + amount;
			try {
				if (message.equals("Deposit")) {
					returnString += " into account ";
					returnString += sourceAccount.getAccountInfo().getAccountNumber();
				} else if (message.equals("Withdraw")) {
					returnString += " from account ";
					returnString += sourceAccount.getAccountInfo().getAccountNumber();
				} else if (message.equals("Transfer")) {
					returnString += " from account ";
					returnString += sourceAccount.getAccountInfo().getAccountNumber();
					returnString += " to account " + targetAccount.getAccountInfo().getAccountNumber();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return returnString;
	}
}
